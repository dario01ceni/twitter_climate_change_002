from transformers import AutoTokenizer, AutoModelForSequenceClassification
from scipy.special import softmax

tweet = "@MehranShakarami today's cold @ home 😒 https://mehranshakarami.com"

#preprocessing, vogliamo trasformare tutti i riferimenti @... in @user

tweet_words = []

for word in tweet.split(' '):
    if word.startswith('@') and len(word) > 1:
        word = '@user'

    elif word.startswith('http'):
        word = "http"

    tweet_words.append(word)

tweet_proc = " ".join(tweet_words)
print(tweet_proc)



# scarichiamo ora il modello e il tokenizer
roberta = "cardiffnlp/twitter-roberta-base-sentiment"

model = AutoModelForSequenceClassification.from_pretrained(roberta)
tokenizer = AutoTokenizer.from_pretrained(roberta)

labels = ['Negative', 'Neutral', 'Positive']


#codice per la sentiment analysis
#convertiamo quanto fatto in un pytorch tensor e poi lo passiamo al modello

encoded_tweet = tokenizer(tweet_proc, return_tensors='pt')
#print(encoded_tweet)
#quello che vediamo dalla print è che encoded_tweet è un dizionario


...

